<?php


class MoreMetadata4CollectionsPlugin extends Omeka_Plugin_AbstractPlugin
{
  const ELEMENT_SET_NAME = 'CustomCollectionMetadata';

  protected $_hooks = array(
			    'initialize',
			    'install',
			    'uninstall',
			    'uninstall_message',
			    'config_form',
			    'config',
			    );

  public function hookInitialize()
  {
    //TODO
    //add_translation_source(dirname(__FILE__) . '/languages');
  }

  public function hookInstall()
  {
    // Don't install if an element set by the name "CustomCollectionMetadata" already exists.
    if ($this->_db->getTable('ElementSet')->findByName(self::ELEMENT_SET_NAME)) {
      throw new Omeka_Plugin_Installer_Exception(
    						 __('An element set by the name "%s" already exists. You must delete '
    						    . 'that element set to install this plugin.', self::ELEMENT_SET_NAME)
    						 );
    }

    $elementSetMetadata = array(
				'name' => self::ELEMENT_SET_NAME,
				'record_type' => 'Collection'
				);
    set_option('metadata4collection_elementSetHeading', self::ELEMENT_SET_NAME);

    //File containing customized metadata
    //TODO: Adapt that to be in the plugin options
    include("custom_metadata.php");


    insert_element_set($elementSetMetadata, $elements);
  }
  public function hookUninstall()
  {
    // Delete the CustomCollectionMetadata element set.
    $this->_db->getTable('ElementSet')->findByName(get_option('metadata4collection_elementSetHeading'))->delete();
  }

  /**                                                                                                                                          
   * Appends a warning message to the uninstall confirmation page.                                                                             
   */
  public function hookUninstallMessage()
  {
    echo '<p>' . __(
            '%1$sWarning%2$s: This will permanently delete the "%3$s" element set %1$sand '
          . 'all defined custom metadata for your collections%2$s. You may deactivate this '
          . 'plugin if you do not want to lose data.',
            '<strong>', '</strong>', get_option('metadata4collection_elementSetHeading')) . '</p>';
  }


  /**                                                                                                                                          
   * Render the config form.                                                                                                                   
   */
  public function hookConfigForm()
  {
    // Set form defaults.                                                                                                                    
    $metatadataForCollectionSetName = get_option('metadata4collection_elementSetHeading');

    echo get_view()->partial(
			     'plugins/more-metadata-for-collection-config-form.php',
			     array('metadata4collection_elementSetHeading' => $metatadataForCollectionSetName)
			     );
  }
  /**     
   * Handle a submitted config form.
   */
  public function hookConfig()
  {
    // Update Element Set Name
    $old_option = get_option('metadata4collection_elementSetHeading');
    $new_option = $_POST['metadata4collection_elementSetHeading'];

    //DB update
    $db = get_db();
    $sql = "UPDATE `{$db->prefix}element_sets` SET `name` = '".$new_option."' WHERE `name` = \"".$old_option."\";";
    $db->query($sql);

    // Set options that are specific to the plugin.
    set_option('metadata4collection_elementSetHeading', $new_option);

  }
}
?>
