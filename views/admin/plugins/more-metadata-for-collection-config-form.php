<div class="field">
    <div class="two columns alpha">
   <label for="scripto_mediawiki_api_url"><?php echo __('Element Set Heading')." ".__('for Custom Metadata Collection Set'); ?></label>
    </div>
    <div class="inputs five columns omega">
   <p class="explanation"><?php echo __('As other element set headings, it could be displayed or not depending on appearance settings'); ?></p>
 <?php echo $this->formText('metadata4collection_elementSetHeading', get_option('metadata4collection_elementSetHeading')); ?>
    </div>
</div>
