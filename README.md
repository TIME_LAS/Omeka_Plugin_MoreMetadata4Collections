# Custom Metadata Set For Collection Omeka Plugin

This plugin allows to add a new set of metadata for collections.

## Requirements

Nothing.

## Warning

List of metadata for the new set is not dynamic. Be careful to well think about it and define all the metadata you need at the 3rd installation step !

## Installation
1. Download the plugin from its GitLab Repository: [Custom Metadata Set For Collection GitLab Repository] and rename the folder to MoreMetadata4Collections

[Custom Metadata Set For Collection GitLab Repository]: https://gitlab.com/TIME_LAS/Omeka_Plugin_MoreMetadata4Collections/]

2. Upload the plugin directory to the plugins directory of your
   Omeka installation. See the [Installing a Plugin] page for details.

3. Customize the list of metadata modifying the file custom_metadata.php

[Installing a Plugin]: http://omeka.org/codex/Installing_a_Plugin

4. Once it's uploaded and unzipped in the `plugins` directory, go to the plugins management interface by clicking on the "Settings"
   tab at the top right of the administrative interface and selecting the "Plugins" tab.

5. Click the green "Install" button in the listing for the plugin.

6. [optional] Customize the Element Set Heading throught the config form of the plugin.